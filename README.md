# Movies

API REST sur la gestion de movies, développé avec Symfony 6.2 et php 8.2

## Setup:
### Prérequis:

- [🐳 docker compose v2.10+](https://docs.docker.com/compose/install/)

### HowTo:

#### Disclaimer:
Je ne suis pas très à l'aise avec Docker, je vais donc sortir le joker "ça fonctionne sur ma machine" (Linux Ubuntu 22)


#### Initier le projet :
- `make init`
- `make mm` (attention, parfois le faire tout de suite après échoue, ne pas 
hésiter à réessayer)

Le projet est ensuite disponible sur [localhost:8092/movies](http://localhost:8092/movies) avec les données fournies.

#### Lancer les tests :
- `make test-setup-db`
- `make test-mm`
- `make test`

#### Autre:

- voir [Makefile](./Makefile) (essayer `make help`)

### Troubleshooting:

Si des ports sont déjà utilisés, modifiez [docker-compose.override.yml](./docker-compose.override.yml) à votre convenance.

### Urls:

- Projet disponible sur http://localhost:8092/movies
- Phpmyadmin sur http://localhost:8093

------------
## Technical Test:

### Specifications:

Ce que j'ai identifié :

- Les opérations de lecture :
  + accessible publiquement => pas de besoin d'authentification
  + large volumes => besoin de pagination
  + gros pics de charge => besoin de cache

- Les opérations d'écriture :
  + accessible de manière privée => besoin d'authentification
  + besoin d'invalider le cache

------------

### Disclaimer: 

Ça peut faire penser à de l'overengineering, mais je pense qu'un test technique est un lieu de démonstration.  
Ce que j'essaie de mettre en avant :
- Architecture de code
- Contrôle des données
- Gestion des erreurs
- Tests

Je n'ai pas eu le temps de tout faire.

### Ce qui est fait :

- `GET /movies` => liste des films paginée et mise en cache évolutif en fonction de la page
```curl
curl --request GET --url 'http://localhost:8092/movies?page=1&itemsPerPage=20'
```
- `GET /movies/{id}` => récupération d'un film avec plus d'info que sur la liste, avec mise en cache plus longue
```curl
curl --request GET --url 'http://localhost:8092/movies/1'
```

#### A partir d'ici, il faut un header 'X-API-Key = iG5lEvI2sL'

- `POST /movies` => ajout d'un film (seulement title et description, pas vraiment eu le temps d'aborder people)
```curl
curl --request POST \
  --url http://localhost:8092/movies \
  --header 'Content-Type: application/json' \
  --header 'X-API-Key: iG5lEvI2sL' \
  --data '{
	"title": "Dune",
	"duration": 216
}'
```
- `PUT /movies` => maj d'un film (mêmes paramètres) avec invalidation du cache du film
```curl
curl --request PUT \
  --url http://localhost:8092/movies/1 \
  --header 'Content-Type: application/json' \
  --header 'X-API-Key: iG5lEvI2sL' \
  --data '{
	"title": "Dune",
	"duration": 216
}'
```
- `DELETE /movies` => suppression d'un film avec invalidation du cache du film
```curl
curl --request DELETE --url http://localhost:8092/movies/1 --header 'X-API-Key: iG5lEvI2sL''
```

### Ce qui n'est pas fait :

- toute la partie people
- la partie 2 du test
- la documentation
- le reste des tests
- l'installation d'outils d'analyse statique et de linter
- Mise en place d'une CI

------------

### Architecture de code

L'architecture du code est inspirée de l'architecture "Ports & Adapter"

------------

### Tests

J'ai écrit quelques tests (très loin d'une couverture idéale par manque de temps), j'ai essayé de démontrer 
plusieurs types de test :
- test aux frontières
- test fonctionnel
- test unitaire

------------

### Documentation

Inexistante par manque de temps
