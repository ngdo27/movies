<?php
declare(strict_types=1);

namespace App\Infrastructure\DrivenAdapter\Storage;

use App\Domain\Command\CreateMovie;
use App\Domain\Command\UpdateMovie;
use App\Domain\Exception\MovieNotFound;
use App\Domain\Ports\MovieProvider;
use App\Domain\Ports\MovieRepository;
use App\Domain\ReadModel\Collection\MovieSummaryCollection;
use App\Domain\ReadModel\Collection\PeopleCollection;
use App\Domain\ReadModel\Movie;
use App\Domain\ReadModel\MovieSummary;
use App\Domain\ReadModel\Pagination\PaginatedMovieSummary;
use App\Domain\ReadModel\People;
use App\Domain\ValueObject\PaginationParameters;

final class SpyMovieStorage implements MovieRepository, MovieProvider
{
    /**
     * @param Movie[] $movies
     * @param MovieSummary[] $movieSummaries
     */
    public function __construct(
        public array $movies = [],
        public array $movieSummaries = []
    ) {}

    public function get(int $id): Movie
    {
        if (isset($this->movies[$id]) === false) {
            throw new MovieNotFound();
        }

        return $this->movies[$id];
    }

    public function paginateMovieSummaries(PaginationParameters $paginationParameters): PaginatedMovieSummary
    {
        $movieSummaries = \array_slice(
            $this->movieSummaries,
            $paginationParameters->itemsPerPage * ($paginationParameters->page - 1),
            $paginationParameters->itemsPerPage
        );

        return new PaginatedMovieSummary(
            $paginationParameters->itemsPerPage,
            \count($this->movieSummaries),
            $paginationParameters->page,
            new MovieSummaryCollection($movieSummaries),
        );
    }

    public function add(CreateMovie $command): int
    {
        $id = \count($this->movies) + 1;
        $this->movies[$id] = new Movie(
            $id,
            $command->title,
            $command->duration,
            new PeopleCollection(),
            [],
        );
        $this->movieSummaries[$id] = new MovieSummary(
            $id,
            $command->title,
            $command->duration,
            [],
            [],
        );

        return $id;
    }

    public function update(UpdateMovie $command): void
    {
        if (isset($this->movies[$command->id], $this->movies[$command->id]) === false) {
            throw new MovieNotFound();
        }

        $previousMovieVersion = $this->movies[$command->id];
        $this->movies[$command->id] = new Movie(
            $command->id,
            $command->title,
            $command->duration,
            $previousMovieVersion->peopleCollection,
            $previousMovieVersion->movieTypeNames
        );

        $previousMovieSummaryVersion = $this->movieSummaries[$command->id];
        $this->movieSummaries[$command->id] = new MovieSummary(
            $command->id,
            $command->title,
            $command->duration,
            $previousMovieSummaryVersion->peopleIds,
            $previousMovieSummaryVersion->movieTypeNames
        );
    }

    public function remove(int $id): void
    {
        if (isset($this->movies[$id], $this->movies[$id])) {
            unset($this->movies[$id]);
            unset($this->movieSummaries[$id]);
        }
    }

    public function updateWithPeopleAndTypes(int $movieId, PeopleCollection $peopleCollection, array $typeNames): void
    {
        $previousMovieVersion = $this->movies[$movieId];
        $this->movies[$movieId] = new Movie(
            $previousMovieVersion->id,
            $previousMovieVersion->title,
            $previousMovieVersion->duration,
            $peopleCollection,
            $typeNames,
        );

        $peopleIds = array_values(array_map(fn(People $people) => $people->id, $peopleCollection->listAll()));

        $previousMovieSummaryVersion = $this->movieSummaries[$movieId];
        $this->movieSummaries[$movieId] = new MovieSummary(
            $previousMovieSummaryVersion->id,
            $previousMovieSummaryVersion->title,
            $previousMovieSummaryVersion->duration,
            $peopleIds,
            $typeNames
        );
    }
}
