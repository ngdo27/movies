<?php
declare(strict_types=1);

namespace App\Infrastructure\DrivenAdapter\Storage;

use App\Domain\Command\CreateMovie;
use App\Domain\Command\UpdateMovie;
use App\Domain\Exception\MovieNotFound;
use App\Domain\Exception\ServiceNotResponding;
use App\Domain\Ports\MovieRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Psr\Log\LoggerInterface;

final readonly class DbalMovieRepository implements MovieRepository
{
    public function __construct(private Connection $connection, private LoggerInterface $logger) {}

    public function add(CreateMovie $command): int
    {
        try {
            $this->connection->insert('movie', [
                'title' => $command->title,
                'duration' => $command->duration,
            ]);

            $id = $this->connection->lastInsertId();

            if ($id === false) {
                throw new Exception('Could not retrieve last idea after movie insertion');
            }

            return (int) $id;
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());

            throw new ServiceNotResponding('MySQL', self::class, $e);
        }
    }

    /** @throws MovieNotFound */
    public function update(UpdateMovie $command): void
    {
        try {
            $result = $this->connection->update('movie', [
                'title' => $command->title,
                'duration' => $command->duration,
            ], ['id' => $command->id]);
            if ($result === 0) {
                throw new MovieNotFound();
            }

        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());

            throw new ServiceNotResponding('MySQL', self::class, $e);
        }
    }

    public function remove(int $id): void
    {
        try {
            $this->connection->delete('movie_has_type', ['Movie_id' => $id]);
            $this->connection->delete('movie_has_people', ['Movie_id' => $id]);
            $this->connection->delete('movie', ['id' => $id]);
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());

            throw new ServiceNotResponding('MySQL', self::class, $e);
        }
    }
}
