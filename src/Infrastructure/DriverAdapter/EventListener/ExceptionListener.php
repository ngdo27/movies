<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\EventListener;

use App\Domain\Exception\ServiceNotResponding;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

final readonly class ExceptionListener
{
    public function __construct(private LoggerInterface $logger) {}

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof ServiceNotResponding) {
            $this->logger->emergency($exception->getMessage());

            // Depending on the situation, we could be less brutal and handling the case at Controller level.
            $event->setResponse(new JsonResponse(null, Response::HTTP_SERVICE_UNAVAILABLE));
        }
    }
}
