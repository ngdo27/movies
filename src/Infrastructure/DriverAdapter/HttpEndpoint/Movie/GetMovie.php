<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\HttpEndpoint\Movie;

use App\Domain\Exception\MovieNotFound;
use App\Domain\Ports\MovieProvider;
use App\Infrastructure\DriverAdapter\HttpEndpoint\Cache\CacheKeyBuilder;
use App\Infrastructure\DriverAdapter\Presenter\MoviePresenter;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

final readonly class GetMovie
{
    private const TIME_24HOURS_IN_SECONDS = 86_400;

    public function __construct(
        private MovieProvider $movieProvider,
        private MoviePresenter $presenter,
        private CacheInterface $cache,
        private LoggerInterface $logger,
        private CacheKeyBuilder $cacheKeyBuilder
    ) {}

    #[Route(path:'/movies/{id}', name: 'get_movie', methods: ['GET'])]
    public function __invoke(int $id): JsonResponse
    {
        try {
            $data = $this->getMovie($id);
        } catch (InvalidArgumentException $exception) {
            $this->logger->warning($exception->getMessage());

            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        } catch (MovieNotFound) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($data);
    }

    /** @throws InvalidArgumentException|MovieNotFound */
    protected function getMovie(int $id): mixed
    {
        return $this->cache->get($this->cacheKeyBuilder->forMovie($id), function (ItemInterface $item) use ($id) {
            $item->expiresAfter(self::TIME_24HOURS_IN_SECONDS);

            $movie = $this->movieProvider->get($id);

            return $this->presenter->expose($movie);
        });
    }
}
