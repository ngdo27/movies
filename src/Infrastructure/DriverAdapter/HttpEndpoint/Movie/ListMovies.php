<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\HttpEndpoint\Movie;

use App\Domain\Exception\InvalidArgumentException;
use App\Domain\Ports\MovieProvider;
use App\Domain\ValueObject\PaginationParameters;
use App\Infrastructure\DriverAdapter\HttpEndpoint\Cache\CacheKeyBuilder;
use App\Infrastructure\DriverAdapter\Presenter\PaginatedMovieSummaryCollectionPresenter;
use Psr\Cache\InvalidArgumentException as CacheInvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

final readonly class ListMovies
{
    private const HOT_CONTENT_TTL_IN_SECONDS = 60;
    private const COLD_CONTENT_TTL_IN_SECONDS = 600;
    private const PAGE_FROM_WHICH_COLD_CONTENT_START = 10;

    public function __construct(
        private MovieProvider $movieProvider,
        private PaginatedMovieSummaryCollectionPresenter $presenter,
        private CacheInterface $cache,
        private LoggerInterface $logger,
        private CacheKeyBuilder $cacheKeyBuilder
    ) {}

    #[Route(path: '/movies', name: 'list_movies', methods: ['GET'])]
    public function __invoke(Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', 1);
        $itemsPerPage = $request->query->getInt('itemsPerPage', 50);

        try {
            $paginationParameters = new PaginationParameters($page, $itemsPerPage);
            $data = $this->cache->get(
                $this->cacheKeyBuilder->forPaginatedList($paginationParameters),
                function (ItemInterface $item) use ($paginationParameters) {
                    $item->expiresAfter($this->computeTTL($paginationParameters->page));

                    $paginatedMovieSummary = $this->movieProvider->paginateMovieSummaries($paginationParameters);

                    return $this->presenter->expose($paginatedMovieSummary);
                }
            );

            return new JsonResponse($data);
        } catch (InvalidArgumentException $exception) {
            return new JsonResponse($exception->errors, Response::HTTP_BAD_REQUEST);
        } catch (CacheInvalidArgumentException $exception) {
            $this->logger->warning($exception->getMessage());

            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }
    }

    private function computeTTL(int $page): int
    {
        if ($page < self::PAGE_FROM_WHICH_COLD_CONTENT_START) {
            return $page * self::HOT_CONTENT_TTL_IN_SECONDS;
        }

        return self::COLD_CONTENT_TTL_IN_SECONDS;
    }
}
