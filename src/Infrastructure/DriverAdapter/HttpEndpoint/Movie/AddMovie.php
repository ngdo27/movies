<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\HttpEndpoint\Movie;

use App\Domain\Command\CreateMovie;
use App\Domain\Command\CreateMovieHandler;
use App\Domain\Exception\InvalidArgumentException;
use App\Infrastructure\DriverAdapter\HttpEndpoint\AuthorizationChecker;
use App\Infrastructure\DriverAdapter\HttpEndpoint\Exception\UnauthorizedException;
use App\Infrastructure\DriverAdapter\HttpEndpoint\Movie\Validator\MovieDataValidator;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final readonly class AddMovie
{
    public function __construct(
        private AuthorizationChecker $authorizationChecker,
        private MovieDataValidator $validator,
        private CreateMovieHandler $createMovieHandler
    ) {}

    #[Route(path: '/movies', name: 'add_movie', methods: ['POST'])]
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $this->authorizationChecker->checkUserAuthorization($request);
            $createMovieCommand = $this->convertRequestToCreateMovieCommand($request);

            return new JsonResponse([
                'id' => $this->createMovieHandler->execute($createMovieCommand),
            ], Response::HTTP_CREATED);
        } catch (InvalidArgumentException $exception) {
            return new JsonResponse($exception->errors, Response::HTTP_BAD_REQUEST);
        } catch (UnauthorizedException) {
            return new JsonResponse(status: Response::HTTP_UNAUTHORIZED);
        }
    }

    /** @throws InvalidArgumentException */
    private function convertRequestToCreateMovieCommand(Request $request): CreateMovie
    {
        try {
            $inputData = $request->toArray();

            $this->validator->validateData($inputData);

            return new CreateMovie(
                $inputData['title'],
                $inputData['duration']
            );
        } catch (JsonException) {
            throw new InvalidArgumentException(['json' => 'malformed json']);
        }
    }
}
