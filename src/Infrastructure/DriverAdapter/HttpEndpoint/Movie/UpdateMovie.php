<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\HttpEndpoint\Movie;

use App\Domain\Command\UpdateMovie as UpdateMovieCommand;
use App\Domain\Command\UpdateMovieHandler;
use App\Domain\Exception\InvalidArgumentException;
use App\Domain\Exception\MovieNotFound;
use App\Infrastructure\DriverAdapter\HttpEndpoint\AuthorizationChecker;
use App\Infrastructure\DriverAdapter\HttpEndpoint\Cache\CacheKeyBuilder;
use App\Infrastructure\DriverAdapter\HttpEndpoint\Exception\UnauthorizedException;
use App\Infrastructure\DriverAdapter\HttpEndpoint\Movie\Validator\MovieDataValidator;
use App\Infrastructure\DriverAdapter\Presenter\MoviePresenter;
use Psr\Cache\InvalidArgumentException as CacheInvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

final readonly class UpdateMovie
{
    public function __construct(
        private AuthorizationChecker $authorizationChecker,
        private MovieDataValidator $validator,
        private UpdateMovieHandler $updateMovieHandler,
        private MoviePresenter $presenter,
        private CacheInterface $cache,
        private CacheKeyBuilder $cacheKeyBuilder,
        private LoggerInterface $logger,
    ) {}

    #[Route(path: '/movies/{id}', name: 'update_movie', methods: ['PUT'])]
    public function __invoke(Request $request, int $id): JsonResponse
    {
        try {
            $this->authorizationChecker->checkUserAuthorization($request);
            $createMovieCommand = $this->convertRequestToUpdateMovieCommand($request, $id);
            $this->cache->delete($this->cacheKeyBuilder->forMovie($id));

            return new JsonResponse(
                $this->presenter->expose($this->updateMovieHandler->execute($createMovieCommand)),
                Response::HTTP_CREATED
            );
        } catch (UnauthorizedException) {
            return new JsonResponse(status: Response::HTTP_UNAUTHORIZED);
        } catch (InvalidArgumentException $exception) {
            return new JsonResponse($exception->errors, Response::HTTP_BAD_REQUEST);
        } catch (MovieNotFound) {
            return new JsonResponse(status: Response::HTTP_NOT_FOUND);
        } catch (CacheInvalidArgumentException $exception) {
            $this->logger->warning($exception->getMessage());

            return new JsonResponse(status: Response::HTTP_NOT_FOUND);
        }
    }

    /** @throws InvalidArgumentException */
    private function convertRequestToUpdateMovieCommand(Request $request, int $id): UpdateMovieCommand
    {
        try {
            $inputData = $request->toArray();
        } catch (JsonException) {
            throw new InvalidArgumentException(['json' => 'malformed json']);
        }

        $this->validator->validateData($inputData);

        return new UpdateMovieCommand(
            $id,
            $inputData['title'],
            $inputData['duration']
        );
    }
}
