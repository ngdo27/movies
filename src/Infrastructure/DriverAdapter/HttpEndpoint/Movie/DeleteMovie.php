<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\HttpEndpoint\Movie;

use App\Domain\Ports\MovieRepository;
use App\Infrastructure\DriverAdapter\HttpEndpoint\AuthorizationChecker;
use App\Infrastructure\DriverAdapter\HttpEndpoint\Cache\CacheKeyBuilder;
use App\Infrastructure\DriverAdapter\HttpEndpoint\Exception\UnauthorizedException;
use Psr\Cache\InvalidArgumentException as CacheInvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

final readonly class DeleteMovie
{
    public function __construct(
        private AuthorizationChecker $authorizationChecker,
        private MovieRepository $movieRepository,
        private CacheInterface $cache,
        private LoggerInterface $logger,
        private CacheKeyBuilder $cacheKeyBuilder
    ) {}

    #[Route(path: '/movies/{id}', name: 'delete_movie', methods: ['DELETE'])]
    public function __invoke(Request $request, int $id): JsonResponse
    {
        try {
            $this->authorizationChecker->checkUserAuthorization($request);
            $this->movieRepository->remove($id);
            $this->cache->delete($this->cacheKeyBuilder->forMovie($id));

            return new JsonResponse(status: Response::HTTP_NO_CONTENT);
        } catch (UnauthorizedException) {
            return new JsonResponse(status: Response::HTTP_UNAUTHORIZED);
        } catch (CacheInvalidArgumentException $exception) {
            $this->logger->warning($exception->getMessage());

            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }
    }
}
