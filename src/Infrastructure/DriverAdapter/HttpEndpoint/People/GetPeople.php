<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\HttpEndpoint\People;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final readonly class GetPeople
{
    #[Route(path:'/people/{id}', name: 'get_people', methods: ['GET'])]
    public function __invoke(int $id): JsonResponse
    {
        // TODO: Not implemented yet, just here to have a people GET url
        return new JsonResponse(['id' => $id]);
    }
}
