<?php
declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\Exception\InvalidArgumentException;

final readonly class UpdateMovie
{
    /** @throws InvalidArgumentException */
    public function __construct(
        public int $id,
        public string $title,
        public int $duration,
    ) {
        $errors = [];
        if ($this->title < 0) {
            $errors['id'] = 'Id cannot be lesser than 0';
        }

        if (\mb_strlen($this->title) > 255) {
            $errors['title'] = 'Title is too long. It cannot be greater than 255 characters long.';
        }

        if ($this->duration < 1) {
            $errors['duration'] = 'Duration cannot be lesser than 1.';
        }

        if ($errors !== []) {
            throw new InvalidArgumentException($errors);
        }
    }
}
