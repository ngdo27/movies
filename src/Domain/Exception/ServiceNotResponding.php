<?php
declare(strict_types=1);

namespace App\Domain\Exception;

final class ServiceNotResponding extends \RuntimeException
{
    public function __construct(string $notRespondingService, string $fullyQualifiedClassName, \Throwable $previous)
    {
        parent::__construct(
            message: "Service $notRespondingService not responding when called by $fullyQualifiedClassName",
            previous: $previous
        );
    }
}
