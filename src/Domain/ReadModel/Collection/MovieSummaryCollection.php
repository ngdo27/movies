<?php
declare(strict_types=1);

namespace App\Domain\ReadModel\Collection;

use App\Domain\ReadModel\MovieSummary;

final class MovieSummaryCollection
{
    /** @param MovieSummary[] $movieSummaries */
    public function __construct(
        private array $movieSummaries = []
    ) {}

    /** @return MovieSummary[] */
    public function listAll(): array
    {
        return $this->movieSummaries;
    }

    public function addMovieSummary(MovieSummary $movie): void
    {
        $this->movieSummaries[] = $movie;
    }

    public function count(): int
    {
        return \count($this->movieSummaries);
    }
}
