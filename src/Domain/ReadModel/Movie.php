<?php
declare(strict_types=1);

namespace App\Domain\ReadModel;

use App\Domain\ReadModel\Collection\PeopleCollection;

final readonly class Movie
{
    /**
     * @param string[] $movieTypeNames
     */
    public function __construct(
        public int $id,
        public string $title,
        public int $duration,
        public PeopleCollection $peopleCollection,
        public array $movieTypeNames,
    ) {}
}
