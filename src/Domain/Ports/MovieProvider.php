<?php

namespace App\Domain\Ports;

use App\Domain\Exception\MovieNotFound;
use App\Domain\ReadModel\Movie;
use App\Domain\ReadModel\Pagination\PaginatedMovieSummary;
use App\Domain\ValueObject\PaginationParameters;

interface MovieProvider
{
    /** @throws MovieNotFound */
    public function get(int $id): Movie;

    public function paginateMovieSummaries(PaginationParameters $paginationParameters): PaginatedMovieSummary;
}
