<?php

namespace App\Domain\Ports;

use App\Domain\Command\CreateMovie;
use App\Domain\Command\UpdateMovie;
use App\Domain\Exception\MovieNotFound;

interface MovieRepository
{
    public function add(CreateMovie $command): int;

    /** @throws MovieNotFound */
    public function update(UpdateMovie $command): void;

    public function remove(int $id): void;
}
