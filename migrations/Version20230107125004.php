<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230107125004 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add provided data for the technical test purpose';
    }

    public function up(Schema $schema): void
    {
        $sql = file_get_contents('data/test-cinemahd-datas.sql');

        $this->addSql($sql);
    }
}
