<?php
declare(strict_types=1);

namespace App\Tests\Unit;

use App\Domain\Exception\InvalidArgumentException;
use App\Domain\ValueObject\PaginationParameters;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertSame;

final class PaginationParametersTest extends TestCase
{
    /** @test */
    public function pagination parameters can be created with valid input(): void
    {
        // Arrange
        $page = 1;
        $itemsPerPage = 4;

        // Act
        $parameters = new PaginationParameters($page, $itemsPerPage);

        // Assert
        self::assertSame(1, $parameters->page);
        self::assertSame(4, $parameters->itemsPerPage);
    }

    /** @test */
    public function pagination parameters can be created with no input(): void
    {
        // Act
        $parameters = new PaginationParameters();

        // Assert
        self::assertSame(1, $parameters->page);
        self::assertSame(50, $parameters->itemsPerPage);
    }

    /** @test */
    public function an exception is thrown when data is invalid(): void
    {
        // Arrange
        $page = 0;
        $itemsPerPage = 0;

        // Act
        try {
            new PaginationParameters($page, $itemsPerPage);

            self::assertTrue(false, 'PaginationParameters should not be created with a page lower than 1');
        } catch (InvalidArgumentException $exception) {
            assertSame([
                '[page]' => 'Should be a number greater than 1',
                '[itemsPerPage]' => 'Should be a number greater than 1',
            ], $exception->errors);
        }
    }
}
