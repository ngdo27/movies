<?php
declare(strict_types=1);

namespace App\Tests\HttpEndpoint;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class AddMovieTest extends WebTestCase
{
    private KernelBrowser $client;
    protected function setUp(): void
    {
        parent::setUp();
        $this->client = self::createClient();
    }

    /** @test */
    public function response is successful with valid provided data and authenticated user(): void
    {
        // Arrange
        $data = ['title' => 'Dune', 'duration' => 216];

        // Act
        $this->doRequest($data);

        // Assert
        self::assertResponseIsSuccessful();
        $response = $this->getJsonDecodedResponse();
        self::assertSame(1, $response['id']);
    }

    /** @test */
    public function not authorized to create movie if not authenticated(): void
    {
        // Arrange
        $data = ['title' => 'Dune', 'duration' => 216];

        // Act
        $this->doRequest($data, false);

        // Assert
        self::assertSame(401, $this->getResponseStatusCode());
        $response = $this->getJsonDecodedResponse();
        self::assertSame([], $response);
    }

    /**
     * @test
     * @dataProvider provideInvalidData()
     */
    public function cannot create movie with invalid arguments(array $invalidData, array $expectedResponse): void
    {
        // Act
        $this->doRequest($invalidData);

        // Assert
        self::assertSame(400, $this->getResponseStatusCode());
        $response = $this->getJsonDecodedResponse();
        self::assertSame($expectedResponse, $response);
    }

    public function provideInvalidData(): array
    {
        return [
            'missing title' => [
                'invalidData' => ['duration' => 210],
                'expectedResponse' => ['[title]' => 'This field is missing.'],
            ],
            'missing duration' => [
                'invalidData' => ['title' => 'Dune'],
                'expectedResponse' => ['[duration]' => 'This field is missing.'],
            ],
            'title is blank' => [
                'invalidData' => ['title' => '', 'duration' => 216],
                'expectedResponse' => ['[title]' => 'This value should not be blank.'],
            ],
        ];
    }

    protected function doRequest(array $data, bool $shouldBeAuthenticated = true): void
    {
        $server = $shouldBeAuthenticated ? ['HTTP_X-API-Key' => 'test-api-key'] : [];

        $this->client->request(
            method: 'POST',
            uri: '/movies',
            server: $server,
            content: \json_encode($data),
        );
    }

    protected function getJsonDecodedResponse(): array
    {
        return \json_decode($this->client->getResponse()->getContent(), true);
    }
    
    protected function getResponseStatusCode(): int
    {
        return $this->client->getResponse()->getStatusCode();
    }
}
