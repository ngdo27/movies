<?php
declare(strict_types=1);

namespace App\Tests\DrivenAdapter;

use App\Domain\Ports\MovieProvider;
use App\Domain\Ports\MovieRepository;
use App\Domain\ReadModel\Collection\PeopleCollection;
use App\Domain\ReadModel\People;
use App\Infrastructure\DrivenAdapter\Storage\SpyMovieStorage;

final class SpyMovieStorageTest extends MovieStorageTest
{
    private SpyMovieStorage $storage;

    protected function setUp(): void
    {
        parent::setUp();

        $this->storage = new SpyMovieStorage();
    }

    protected function getMovieRepository(): MovieRepository
    {
        return $this->storage;
    }

    protected function getMovieProvider(): MovieProvider
    {
        return $this->storage;
    }

    protected function insertPeopleAndTypeForMovie(int $movieId, int $numberOfPeople, int $numberOfTypes): void
    {
        $nationalities = ['anglaise', 'japonaise', 'marocaine', 'péruvienne', 'iranienne'];
        $role = ['actrice', 'réalisatrice', 'productrice'];
        $significance = ['principal', 'secondaire', null];

        $peopleCollection = new PeopleCollection();
        for ($i = 1; $i < $numberOfPeople; $i++) {
            $peopleCollection->addPeople(new People(
                $i,
                "firstname$i",
                "lastname$i",
                "1985-08-$i",
                $nationalities[rand(0, 4)],
                $role[rand(0, 2)],
                $significance[rand(0, 2)],
            ));
        }

        $types = ['fantastique', 'horreur', 'science-fiction', 'policier'];
        $typeNames = [];

        for ($i = 0; $i < $numberOfTypes; $i++) {
            $typeNames[] = $types[$i];
        }

        $this->storage->updateWithPeopleAndTypes($movieId, $peopleCollection, $typeNames);
    }
}
