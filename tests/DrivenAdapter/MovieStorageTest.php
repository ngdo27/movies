<?php
declare(strict_types=1);

namespace App\Tests\DrivenAdapter;

use App\Domain\Command\CreateMovie;
use App\Domain\Command\UpdateMovie;
use App\Domain\Exception\MovieNotFound;
use App\Domain\Ports\MovieProvider;
use App\Domain\Ports\MovieRepository;
use App\Domain\ValueObject\PaginationParameters;
use function PHPUnit\Framework\assertSame;

abstract class MovieStorageTest extends IntegratedTestCase
{
    abstract protected function getMovieRepository(): MovieRepository;
    abstract protected function getMovieProvider(): MovieProvider;

    abstract protected function insertPeopleAndTypeForMovie(int $movieId, int $numberOfPeople, int $numberOfTypes): void;

    /** @test */
    public function adding a movie successfully(): void
    {
        // Arrange
        $title = 'Dune';
        $duration = 216;
        $command = new CreateMovie($title, $duration);

        // Act
        $id = $this->getMovieRepository()->add($command);
        $movie = $this->getMovieProvider()->get($id);

        // Assert
        self::assertSame($id, $movie->id);
        self::assertSame('Dune', $movie->title);
        self::assertSame(216, $movie->duration);
    }

    /** @test */
    public function detecting that a movie is not in storage(): void
    {
        // Expect
        self::expectException(MovieNotFound::class);

        // Arrange
        $id = 1;

        // Act
        $this->getMovieProvider()->get($id);
    }

    /** @test */
    public function updating a movie successfully(): void
    {
        // Arrange
        $id = $this->getMovieRepository()->add(new CreateMovie('Dneu', 612));
        $fixedTitle = 'Dune';
        $fixedDuration = 216;

        // Act
        $this->getMovieRepository()->update(new UpdateMovie($id, $fixedTitle, $fixedDuration));
        $movie = $this->getMovieProvider()->get($id);

        // Assert
        self::assertSame($id, $movie->id);
        self::assertSame('Dune', $movie->title);
        self::assertSame(216, $movie->duration);
    }

    /** @test */
    public function cannot update inexistant movie(): void
    {
        // Expect
        self::expectException(MovieNotFound::class);

        // Arrange
        $id = 1;
        $fixedTitle = 'Dune';
        $fixedDuration = 216;

        // Act
        $this->getMovieRepository()->update(new UpdateMovie($id, $fixedTitle, $fixedDuration));
    }

    /** @test */
    public function removing a movie successfully(): void
    {
        // Expect
        self::expectException(MovieNotFound::class);

        // Arrange
        $id = $this->getMovieRepository()->add(new CreateMovie('Dneu', 612));

        // Act
        $this->getMovieRepository()->remove($id);
        $this->getMovieProvider()->get($id);
    }

    /** @test */
    public function get a paginated movie list successfully(): void
    {
        // Arrange
        $id = $this->getMovieRepository()->add(new CreateMovie('Dune', 216));
        $this->insertPeopleAndTypeForMovie($id, 3, 2);

        $this->getMovieRepository()->add(new CreateMovie('LotR 1', 178));
        $this->getMovieRepository()->add(new CreateMovie('LotR 2', 179));
        $this->getMovieRepository()->add(new CreateMovie('LotR 3', 201));

        // Act
        $paginatedMovieList = $this->getMovieProvider()->paginateMovieSummaries(new PaginationParameters(itemsPerPage: 1));

        // Assert
        self::assertSame(1, $paginatedMovieList->itemsPerPage);
        self::assertSame(4, $paginatedMovieList->totalItems);
        self::assertSame(1, $paginatedMovieList->currentPage);
        self::assertSame($id, $paginatedMovieList->collection->listAll()[0]->id);
        self::assertSame('Dune', $paginatedMovieList->collection->listAll()[0]->title);
        self::assertSame(216, $paginatedMovieList->collection->listAll()[0]->duration);
        self::assertSame([1, 2], $paginatedMovieList->collection->listAll()[0]->peopleIds);
        self::assertSame(['fantastique', 'horreur'], $paginatedMovieList->collection->listAll()[0]->movieTypeNames);
    }
}
