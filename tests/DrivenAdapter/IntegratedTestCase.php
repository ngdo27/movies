<?php
declare(strict_types=1);

namespace App\Tests\DrivenAdapter;

use Doctrine\DBAL\Connection;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class IntegratedTestCase extends KernelTestCase
{
    protected Connection $connection;
    protected LoggerInterface $logger;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = self::getContainer()->get(Connection::class);
        $this->logger = new NullLogger();

        $this->connection->beginTransaction();
    }

    protected function tearDown(): void
    {
        $this->connection->rollBack();
        parent::tearDown();
    }
}
